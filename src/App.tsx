import React from 'react';
import {
  Route,
  Routes,
} from "react-router-dom";
import { Container, Nav, Navbar } from 'react-bootstrap';
import HomePage from './pages/HomePage';
import LoginPage from './pages/Login';
import Register from './pages/Register';
import NotFound from './pages/404';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  return (
    <>
      <Navbar bg="light" data-bs-theme="light">
        <Container>
        <Navbar.Brand href="/">
            <img
              src="https://otus.ru/_next/static/images/img/logo2022_without_text-ad6a01e8608432b9967c441a0f4760b4.svg"
              width="50"
              height="50"
              className="d-inline-block align-top"
              alt="React Bootstrap logo"
            />
          </Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href="login">Авторизация</Nav.Link>
            <Nav.Link href="register">Регистрация</Nav.Link>
          </Nav>
        </Container>
      </Navbar>

      <Routes>
        <Route index element={<HomePage />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/register" element={<Register />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </>
  );
}

export default App;
