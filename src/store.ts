import { configureStore } from '@reduxjs/toolkit';
import userReducer from './stateManagemet/userReducer';

const store= configureStore({
    reducer: {
        user: userReducer,
    },
   
});
export default store;