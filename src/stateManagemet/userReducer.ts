import { User } from "../models/user";

export const UserState = Object.freeze({
    USER_SET: '[USER_STATE] USER_SET',
});


export const Actions = Object.freeze({
    setUser: (user: User) => ({ type: UserState.USER_SET, payload: user })
});

interface State {
    user: User | null;
}

const initialState: State = {
    user: null,
}

const userReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case UserState.USER_SET:
    
            return { ...state, user: action.payload };
        default:
            return state;
    }
};
export default userReducer;