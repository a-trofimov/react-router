import { connect } from "react-redux";
import { User } from "../../models/user";

interface Props {
    user: User | null;
}

const HomePage = (props: Props) => {

    const { user } = props;

    const css = {
        padding: '10px',
        border: '5px solid blue'
    };


    return (
        <div style={css}>
            <span>Главная</span>
            <br />
            {user !== null && <span>Ваша почта: {user.username}</span>}
        </div>
    )



};

const mapStateToProps = (state: any) => {
    return { user: state.user.user };
}

export default connect(mapStateToProps)(HomePage);