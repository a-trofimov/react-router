import { Form, Button, Row, Col, InputGroup } from 'react-bootstrap';
import './index.css';

const Register = () => {
    return (
        <div className="login-form">
            <Form>
                <Row className="mb-3">
                    <Form.Group as={Col} md="4" controlId="validationCustom01">
                        <Form.Label>Имя</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Имя"
                        />
                        <Form.Control.Feedback />
                    </Form.Group>
                    <Form.Group as={Col} md="4" controlId="validationCustom02">
                        <Form.Label>Фамилия</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Фамилия"
                        />
                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="4" controlId="validationCustomUsername">
                        <Form.Label>Login</Form.Label>
                        <InputGroup hasValidation>
                            <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
                            <Form.Control
                                type="text"
                                placeholder="Login"
                                aria-describedby="inputGroupPrepend"
                                required
                            />
                            <Form.Control.Feedback type="invalid" />
                        </InputGroup>
                    </Form.Group>
                </Row>
                <Row className="mb-2">
                    <Form.Group as={Col} md="8" controlId="validationCustom03">
                        <Form.Label>Город</Form.Label>
                        <Form.Control type="text" placeholder="Город" required />
                        <Form.Control.Feedback type="invalid" />
                    </Form.Group>
                    <Form.Group as={Col} md="4" controlId="validationCustom05">
                        <Form.Label>Индекс</Form.Label>
                        <Form.Control type="text" placeholder="Индекс" required />
                        <Form.Control.Feedback type="invalid" />
                    </Form.Group>
                </Row>
                <Button className="btn-submit" type="submit">Сохранить</Button>
            </Form>
        </div>
    )
};

export default Register;