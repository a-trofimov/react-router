import { useState } from 'react';
import { useDispatch } from "react-redux";
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from "react-router-dom";
import { Actions } from "../../stateManagemet/userReducer";
import './index.css';

const Login = () => {
    const [validated, setValidated] = useState(false);
    const [username, setUsername] = useState("");
    const navigate = useNavigate();
    const dispatch = useDispatch();


    const login = (event: any) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        else {
            dispatch(Actions.setUser({ username: username }));
            navigate('/home');
        }

        setValidated(true);
    }

    return (
        <div className="login-form">
            <Form noValidate validated={validated} onSubmit={login}>
                <Form.Group className="mb-3" controlId="email">
                    <Form.Label>Почта</Form.Label>
                    <Form.Control required type="email" placeholder="name@example.com" onChange={e => setUsername(e.target.value)} />
                    <Form.Control.Feedback type="invalid" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Пароль</Form.Label>
                    <Form.Control required type="password" placeholder="Введите пароль" />
                    <Form.Control.Feedback type="invalid" />
                </Form.Group>
                <Button type="submit">Войти</Button>
            </Form>
        </div>
    )
};

export default Login;